```json:table
{
    "fields":[
        {"key":"a", "lablel":"AA", "sortable":true},
        {"key":"b", "lablel":"BB", "sortable":true},
        {"key":"c", "lablel":"CC", "sortable":true},
        {"key":"d", "lablel":"DD", "sortable":true}
    ],
    "items":[
        {"a":"11", "b":"22", "c":"33", "d":"44"},
        {"a":"peanuts", "b":"22", "c":"33", "d":"44"},
        {"a":"11", "b":"22", "c":"crackerjack", "d":"44"},
        {"a":"11", "b":"don't care", "c":"33", "d":"if I come back"}
    ],
    "filter": true
}
```
